#!/usr/bin/env python3
from flask import Flask, request, jsonify, Response
import mysql.connector
from flask_cors import CORS, cross_origin
from decimal import *
from prometheus_flask_exporter import PrometheusMetrics

app = Flask(__name__)
# PrometheusMetrics(app)

metrics = PrometheusMetrics(app)
metrics.info('app_info', 'Application info', version='1.0.3')


cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

conf = {
    #  'user': 'andrei',
    #  'password': 'andrei',
    'user': 'root',
    'password': 'root',
    #  'host': 'localhost'
    'host': 'db',
    'database': 'project_db',
    'port': '3306',
    'raise_on_warnings': True,
    # line below for docker-compose
    'auth_plugin': 'mysql_native_password',
}

@app.route("/")
# @cross_origin()
def test():
    # return get_team_standings()
    #  return "TEST OK - DOCKER OK 3"
     return jsonify("TEST OK"), 200


@app.route("/getTeamStandings")
# @cross_origin()
def get_team_standings():
    result = []
    mydb = mysql.connector.connect(**conf)
    cursor = mydb.cursor()
    # cursor.execute('select * from teams')
    cursor.callproc('print_teams');
    for f in cursor.stored_results():
        result.append(f.fetchall())

    print("result = ", result, "\n\n")
    # remove id
    final_result = result[0]

    print("final result = ", final_result, "\n\n")

    if result is None:
        return jsonify([]), 400
    # return "OK", 200
    return jsonify(final_result), 200

@app.route("/getPlayer/<name>")
# @cross_origin()
def get_player(name):
    print("name = ", name)
    result = []
    mydb = mysql.connector.connect(**conf)
    cursor = mydb.cursor()
    cursor.callproc('get_player', [name])
    for f in cursor.stored_results():

        result.append(f.fetchall())
    print("result = ", result)

    if result is None:
        return jsonify([]), 400
    # return "OK", 200
    result[0][0] = tuple([str(i) for i in result[0][0]])

    return jsonify(result), 200


@app.route("/getMatch/<id>")
# @cross_origin()
def get_Match(id):
    print("id = ", id)
    result = []
    mydb = mysql.connector.connect(**conf)
    cursor = mydb.cursor()
    cursor.callproc('get_match', [id])
    for f in cursor.stored_results():
        result.append(f.fetchall())
    print("result = ", result)

    final_result = result
    # final_result = [r[0] for r in result]
    if final_result is None:
        return jsonify([]), 400
    # return "OK", 200
    return jsonify(final_result), 200


@app.route("/getTeam/<name>")
# @cross_origin()
def get_Team(name):
    print("team name = ", name)
    result = []
    mydb = mysql.connector.connect(**conf)
    cursor = mydb.cursor()
    cursor.callproc('get_team', [name])
    for f in cursor.stored_results():

        result.append(f.fetchall())
    print("result = ", result)

    if result is None:
        return jsonify([]), 400
    # return "OK", 200
    return jsonify(result), 200

@app.route("/topScorers")
# @cross_origin()
def top_scorers():
    result = []
    mydb = mysql.connector.connect(**conf)
    cursor = mydb.cursor()
    cursor.callproc('top_scorers')

    for f in cursor.stored_results():
        result.append(f.fetchall())

    print("result = ", result)
    final_result = result[0]
    if final_result is None:
        return jsonify([]), 400
    # return "OK", 200
    final_result = [[str(j) for j in i] for i in final_result]

    return jsonify(final_result), 200


@app.route("/getPlayerNames")
# @cross_origin()
def get_player_names():
    result = []
    mydb = mysql.connector.connect(**conf)
    cursor = mydb.cursor()
    cursor.callproc('get_player_names');
    for f in cursor.stored_results():
        result.append([i[0] for i in f.fetchall()])

    print("result = ", result, "\n\n")
    final_result = result[0]

    print("final result = ", final_result, "\n\n")

    if result is None:
        return jsonify([]), 400
    # return "OK", 200
    return jsonify(final_result), 200

@app.route("/getMatchesList")
# @cross_origin()
def get_matches_list():
    result = []
    mydb = mysql.connector.connect(**conf)
    cursor = mydb.cursor()
    cursor.callproc('get_matches_list');
    for f in cursor.stored_results():
        result.append(f.fetchall())

    print("result = ", result, "\n\n")
    final_result = result[0]

    print("final result = ", final_result, "\n\n")

    if result is None:
        return jsonify([]), 400
    # return "OK", 200
    return jsonify(final_result), 200


@app.route("/getTeamNames")
# @cross_origin()
def get_team_names():
    result = []
    mydb = mysql.connector.connect(**conf)
    cursor = mydb.cursor()
    cursor.callproc('get_team_names');
    for f in cursor.stored_results():
        result.append([i[0] for i in f.fetchall()])

    print("result = ", result, "\n\n")
    final_result = result[0]

    print("final result = ", final_result, "\n\n")

    if result is None:
        return jsonify([]), 400
    # return "OK", 200
    return jsonify(final_result), 200





if __name__ == '__main__':
    # app.run(debug=True, host='0.0.0.0', port=5000)
    app.run('0.0.0.0', 5000)


