create database if not exists project_db;
use project_db;

SET GLOBAL log_bin_trust_function_creators = 1;

-- ============================================= TABLES
SET foreign_key_checks = 0;

drop table if exists teams;
create table teams (
    team_id INT AUTO_INCREMENT PRIMARY KEY,
    team_name varchar(128) NOT NULL,
    goals_for INT DEFAULT 0,
    goals_against INT DEFAULT 0,
    points INT DEFAULT 0,
    matches INT DEFAULT 0,
    country varchar(128) NOT NULL,
    FOREIGN KEY(country) REFERENCES leagues(country)
);


drop table if exists players;
create table players (
    player_id INT AUTO_INCREMENT PRIMARY KEY,
    player_name varchar(128) NOT NULL,
    team_id INT,
    goals INT DEFAULT 0,
    red_cards INT DEFAULT 0,
    matches INT DEFAULT 0,
    age INT,
    position varchar(128) NOT NULL,
    FOREIGN KEY (team_id) REFERENCES teams(team_id),
    CHECK(age >= 0)
);

drop table if exists matches;
create table matches (
    match_id INT AUTO_INCREMENT PRIMARY KEY,
    team1_id INT,
    team2_id INT,
    goals1 INT,
    goals2 INT,
    match_date DATE,
    possesion1 INT,
    possesion2 INT,
    FOREIGN KEY(team1_id) REFERENCES teams(team_id),
    FOREIGN KEY(team2_id) REFERENCES teams(team_id),
    CHECK(goals1 >= 0),
    CHECK(goals2 >= 0),
    CHECK(possesion1 >= 0 and possesion1 <= 100),
    CHECK(possesion2 >= 0 and possesion2 <= 100 and possesion1 + possesion2 = 100)
);

-- match event
-- type = G (goal) / C (red card)
drop table if exists match_events;
create table match_events (
    event_id INT AUTO_INCREMENT PRIMARY KEY,
    match_id INT,
    team_id INT,
    player_id INT,
    event_type varchar(1),
    minute INT,
    FOREIGN KEY (team_id) REFERENCES teams(team_id),
    FOREIGN KEY (player_id) REFERENCES players(player_id),
    FOREIGN KEY (match_id) REFERENCES matches(match_id),
    CHECK (event_type in ("C", "G")),
    CHECK(minute >= 0 and minute <= 120)
);

drop table if exists leagues;
create table leagues (
    country varchar(128) PRIMARY KEY NOT NULL,
    most_leagues_team varchar(128) NOT NULL,
    most_cups_team varchar(128) NOT NULL,
    last_champion_team varchar(128) NOT NULL,
    league_name varchar(128) NOT NULL
);

SET foreign_key_checks = 1;

-- ============================================= TRIGGERS

DELIMITER //

drop trigger if exists new_match_trigger;
create trigger new_match_trigger
    after INSERT
    on matches FOR EACH ROW 
BEGIN
    DECLARE p1 INT;
    DECLARE p2 INT;
    
    IF NEW.goals1 = NEW.goals2 THEN
        SET p1 := 1;
        SET p2 := 1;
    ELSEIF NEW.goals1 > NEW.goals2 THEN
        SET p1 := 3;
        SET p2 := 0;
    ELSE
        SET p1 := 0;
        SET p2 := 3;
    END IF;

    update teams 
        set points = points + p1, goals_for = goals_for + NEW.goals1, goals_against = goals_against + NEW.goals2,
            matches = matches + 1
        where team_id = NEW.team1_id;
    
    update teams 
        set points = points + p2, goals_for = goals_for + NEW.goals2, goals_against = goals_against + NEW.goals1,
            matches = matches + 1
        where team_id = NEW.team2_id;

    update players
        set matches = matches + 1
        where team_id = NEW.team1_id or team_id = NEW.team2_id;
END //


drop trigger if exists new_event_trigger;
create trigger new_event_trigger
    after INSERT 
    on match_events FOR EACH ROW
BEGIN
    IF NEW.event_type = "G" THEN
        update players
            set goals = goals + 1
            where player_id = NEW.player_id;
    ELSE
        update players
            set red_cards = red_cards + 1
            where player_id = NEW.player_id;
    END IF;
END //






-- ============================================= FUNCTIONS AND PROCEDURES


-- helper functions


drop function if exists get_player_id;
create function get_player_id(p_name varchar(128))
returns INT
BEGIN
    return (select player_id from players where player_name = p_name);
    
END //


drop function if exists get_player_name;
create function get_player_name(p_id INT)
returns varchar(128)
BEGIN
    return (select player_name from players where player_id = p_id);
    
END //


drop function if exists get_team_id;
create function get_team_id(t_name varchar(128))
returns INT
BEGIN
    return (select team_id from teams where team_name = t_name);
    
END //

drop function if exists get_team_name;
create function get_team_name(t_id INT)
returns varchar(128)
BEGIN
    return (select team_name from teams where team_id = t_id);
END //


drop function if exists get_match_id;
create function get_match_id(t_name varchar(128), m_date DATE)
returns INT
BEGIN
    DECLARE t_id INT;
    SET t_id = get_team_id(t_name);
    return (select match_id from matches where match_date = m_date and 
    (t_id = team1_id or t_id = team2_id));
END //

-- procedures for adding inside tables

drop procedure if exists add_team;
create procedure add_team(IN team_name varchar(128), IN country varchar(128))
BEGIN
    insert into teams(team_name, country) values (team_name, country);
END //


drop procedure if exists add_player;
create procedure add_player(
    IN player_name varchar(128),
    IN team_name varchar(128),
    IN age INT,
    IN position varchar(128))
BEGIN
    insert into players(player_name, team_id, age, position) VALUES (
        player_name,
        get_team_id(team_name),
        age,
        position);
END //


drop procedure if exists add_match;
create procedure add_match(
    IN team1 varchar(128),
    IN team2 varchar(128),
    IN goals1 INT,
    IN goals2 INT,
    IN match_date DATE,
    IN possesion1 INT,
    IN possesion2 INT)
BEGIN
    insert into matches(team1_id, team2_id, goals1, goals2, match_date, possesion1, possesion2) VALUES (
        get_team_id(team1),
        get_team_id(team2),
        goals1,
        goals2,
        match_date,
        possesion1,
        possesion2);

END //



-- needed to determine match - team and date (team can be either team1 or team2, only one match / day)

drop procedure if exists add_match_event;
create procedure add_match_event(
    IN team varchar(128),
    IN match_date DATE,
    IN player_name varchar(128),
    IN event_type varchar(1),
    IN minute INT)
BEGIN
    insert into match_events (match_id, team_id, player_id, event_type, minute) VALUES(
        get_match_id(team, match_date),
        get_team_id(team),
        get_player_id(player_name),
        event_type,
        minute
    );
END //

-- ===================================================================
-- ===================================================================
-- ===================================================================
-- ===================================================================
-- API FUNCTIONS


drop procedure if exists print_teams;
create procedure print_teams()
BEGIN
    select team_name, points, goals_for, goals_against, matches, country
    from teams order by points desc, goals_for desc, goals_against;

END //

drop procedure if exists get_player_names;
create procedure get_player_names()
BEGIN
    select player_name from players;

END //

drop procedure if exists get_player;
create procedure get_player(IN player_name varchar(128))
BEGIN
    DECLARE p_id INT;
    DECLARE t_id INT;
    select get_player_id(player_name) into p_id;
    select team_id into t_id from players where player_id = p_id;
    
    -- player info
    select player_name, goals, red_cards, matches, age, position, 1.0*goals/matches
        from players where player_id = p_id;
    
    -- team info
    select team_name, country from teams where team_id = t_id;

    -- last 3 matches
    select match_date , get_team_name(team1_id), get_team_name(team2_id), CONCAT(`goals1`, ' - ', `goals2`)
        from matches where team1_id = t_id or team2_id = t_id order by match_date desc LIMIT 3;
END //


drop procedure if exists top_scorers;
create procedure top_scorers()
BEGIN
    select player_name, get_team_name(team_id), goals, red_cards, matches,
        age, position, 1.0*goals/matches from players where goals > 0 order by goals desc;

END //

drop procedure if exists get_team_names;
create procedure get_team_names()
BEGIN
    select team_name, team_id from teams;

END //

drop procedure if exists get_matches_list;
create procedure get_matches_list()
BEGIN
    select match_id, CONCAT(get_team_name(team1_id), ' - ', get_team_name(team2_id), ', ', match_date) from matches;
END //


drop procedure if exists get_match;
create procedure get_match(IN m_id INT)
BEGIN
    select CONCAT(get_team_name(team1_id), ' - ', get_team_name(team2_id)), match_date, 
        CONCAT(goals1, ' - ', goals2), CONCAT(possesion1, ' - ', possesion2) from matches
        where match_id = m_id;
    select get_team_name(team_id), get_player_name(player_id), event_type, minute from match_events
        where match_id = m_id order by minute;
END //

drop procedure if exists get_team;
create procedure get_team(IN team_name varchar(128))
BEGIN
    DECLARE t_id INT;
    select get_team_id(team_name) into t_id;
    -- team info
    select team_name, goals_for, goals_against, points, matches, country from teams where team_id = t_id;

    -- all players from the team
    select player_name, goals, age, position from players where team_id = t_id;

    -- last 3 matches
    select match_date , get_team_name(team1_id), get_team_name(team2_id), CONCAT(`goals1`, ' - ', `goals2`)
        from matches where team1_id = t_id or team2_id = t_id order by match_date desc LIMIT 3;

END //










-- procedures for printing


drop procedure if exists print_players;
create procedure print_players()
BEGIN
    select * from players;

END //

drop procedure if exists print_matches;
create procedure print_matches()
BEGIN
    select * from matches;
END //

drop procedure if exists print_events;
create procedure print_events()
BEGIN
    select * from match_events;
END //


drop procedure if exists print_all;
create procedure print_all()
BEGIN
    CALL print_teams;
    CALL print_players;
    CALL print_matches;
    CALL print_events;
END //







DELIMITER ;

-- ============================================= INIT VALUES


insert into leagues(country, most_leagues_team, most_cups_team, last_champion_team, league_name) VALUES
("Spain", "Real Madrid", "Barcelona", "Real Madrid", "La Liga");

insert into leagues(country, most_leagues_team, most_cups_team, last_champion_team, league_name)VALUES
("France", "PSG", "PSG", "PSG", "Ligue 1");

insert into leagues(country, most_leagues_team, most_cups_team, last_champion_team, league_name)VALUES
("England", "Manchester United", "Liverpool", "Manchester City", "Premier League");

CALL add_team("Real Madrid", "Spain");
CALL add_team("Barcelona", "Spain");
CALL add_team("PSG", "France");
CALL add_team("Liverpool", "England");
CALL add_team("Manchester City", "England");


CALL add_player("Ronaldo", "Real Madrid", 34, "striker");
CALL add_player ("Benzema", "Real Madrid", 31, "striker");
CALL add_player ("Modric", "Real Madrid", 33, "midfielder");
CALL add_player ("Sergio Ramos", "Real Madrid", 34, "defender");
CALL add_player ("Hazard", "Real Madrid", 29, "midfielder");

CALL add_player ("Messi", "Barcelona", 32, "striker");
CALL add_player ("Rakitic", "Barcelona", 31, "midfielder");
CALL add_player ("Busquets", "Barcelona", 31, "midfielder");
CALL add_player ("Suarez", "Barcelona", 32, "striker");
CALL add_player ("Pique", "Barcelona", 33, "defender");

CALL add_player ("Neymar", "PSG", 28, "striker");
CALL add_player ("Mbappe", "PSG", 18, "striker");
CALL add_player ("Veratti", "PSG", 27, "midfielder");
CALL add_player ("Thiago Silva", "PSG", 33, "defender");
CALL add_player ("Di Maria", "PSG", 28, "striker");


CALL add_player ("Salah", "Liverpool", 28, "striker");
CALL add_player ("Mane", "Liverpool", 27, "striker");
CALL add_player ("Fabinho", "Liverpool", 25, "midfielder");
CALL add_player ("Van Dijk", "Liverpool", 28, "defender");
CALL add_player ("Firmino", "Liverpool", 27, "striker");

CALL add_player ("Raheem Sterling", "Manchester City", 28, "striker");
CALL add_player ("David Silva", "Manchester City", 27, "midfielder");
CALL add_player ("De Bruyne", "Manchester City", 25, "midfielder");
CALL add_player ("Laporte", "Manchester City", 28, "defender");
CALL add_player ("Kun Aguero", "Manchester City", 27, "striker");




CALL add_match("Real Madrid", "Barcelona", 3, 0, "2019-10-14", 53, 47);
CALL add_match_event("Real Madrid", "2019-10-14", "Ronaldo", "G", 30);
CALL add_match_event("Real Madrid", "2019-10-14", "Ronaldo", "G", 44);
CALL add_match_event("Real Madrid", "2019-10-14", "Ronaldo", "G", 78);
CALL add_match_event("Real Madrid", "2019-10-14", "Sergio Ramos", "C", 88);


CALL add_match("Real Madrid", "Liverpool", 0, 0, "2019-11-10", 50, 50);

CALL add_match("Manchester City", "Real Madrid", 2, 1, "2019-11-24", 60, 40);
CALL add_match_event("Manchester City", "2019-11-24", "De Bruyne", "G", 10);
CALL add_match_event("Manchester City", "2019-11-24", "Kun Aguero", "G", 15);
CALL add_match_event("Real Madrid", "2019-11-24", "Ronaldo", "G", 89);

CALL add_match("PSG", "Real Madrid", 1, 2, "2019-10-27", 50, 50);
CALL add_match_event("Real Madrid", "2019-10-27", "Ronaldo", "G", 40);
CALL add_match_event("Real Madrid", "2019-10-27", "Hazard", "G", 50);
CALL add_match_event("PSG", "2019-10-27", "Mbappe", "G", 60);


CALL add_match("Barcelona", "PSG", 0, 0, "2019-11-5", 40, 60);
CALL add_match_event("Barcelona", "2019-11-5", "Messi", "C", 63);

CALL add_match("PSG", "Manchester City", 2, 0, "2019-11-20", 63, 37);
CALL add_match_event("PSG", "2019-11-20", "Neymar", "G", 22);
CALL add_match_event("PSG", "2019-11-20", "Neymar", "G", 74);

CALL add_match("Liverpool", "PSG", 2, 2, "2019-11-27", 49, 51);
CALL add_match_event("Liverpool", "2019-11-27", "Salah", "G", 33);
CALL add_match_event("Liverpool", "2019-11-27", "Salah", "G", 76);
CALL add_match_event("Liverpool", "2019-11-27", "Van Dijk", "C", 78);
CALL add_match_event("PSG", "2019-11-27", "Mbappe", "G", 44);
CALL add_match_event("PSG", "2019-11-27", "Mbappe", "G", 58);

CALL add_match("Barcelona", "Liverpool", 3, 1, "2019-10-23", 62, 38);
CALL add_match_event("Barcelona", "2019-10-23", "Messi", "G", 11);
CALL add_match_event("Barcelona", "2019-10-23", "Messi", "G", 26);
CALL add_match_event("Barcelona", "2019-10-23", "Messi", "G", 55);
CALL add_match_event("Barcelona", "2019-10-23", "Pique", "C", 80);
CALL add_match_event("Liverpool", "2019-10-23", "Van Dijk", "G", 88);

CALL add_match("Manchester City", "Barcelona", 1, 1, "2019-11-10", 51, 49);
CALL add_match_event("Barcelona", "2019-11-10", "Messi", "G", 60);
CALL add_match_event("Manchester City", "2019-11-10", "David Silva", "G", 78);

CALL add_match("Liverpool", "Manchester City", 0, 0, "2019-12-7", 49, 51);


